/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import hilog from '@ohos.hilog';
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'
import { PullToRefresh, PullToRefreshConfigurator } from '@ohos/pulltorefresh'


export default function abilityTest() {
  describe('ActsAbilityTest', ()=> {
    // Defines a test suite. Two parameters are supported: test suite name and test suite function.
    beforeAll(()=> {
      // Presets an action, which is performed only once before all test cases of the test suite start.
      // This API supports only one parameter: preset action function.
    })
    beforeEach(()=> {
      // Presets an action, which is performed before each unit test case starts.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: preset action function.
    })
    afterEach(()=> {
      // Presets a clear action, which is performed after each unit test case ends.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: clear action function.
    })
    afterAll(()=> {
      // Presets a clear action, which is performed after all test cases of the test suite end.
      // This API supports only one parameter: clear action function.
    })


    it('TestPullToRefreshConfigurator', 0, ()=> {
      let refreshConfigurator: PullToRefreshConfigurator = new PullToRefreshConfigurator();
      // 设置属性
      refreshConfigurator
        .setHasRefresh(true) // 是否具有下拉刷新功能
        .setHasLoadMore(true) // 是否具有上拉加载功能
        .setMaxTranslate(150) // 可下拉上拉的最大距离
        .setSensitivity(1) // 下拉上拉灵敏度
        .setListIsPlacement(false) // 滑动结束后列表是否归位
        .setAnimDuration(300) // 滑动结束后，回弹动画执行时间
        .setRefreshHeight(80) // 下拉动画高度
        .setRefreshColor('#ff0000') // 下拉动画颜色
        .setRefreshBackgroundColor('#ffbbfaf5') // 下拉动画区域背景色
        .setRefreshTextColor('red') // 下拉加载完毕后提示文本的字体颜色
        .setRefreshTextSize(25) // 下拉加载完毕后提示文本的字体大小
        .setRefreshAnimDuration(1000) // 下拉动画执行一次的时间
        .setLoadImgHeight(50) // 上拉图片高度
        .setLoadBackgroundColor('#ffbbfaf5') // 上拉动画区域背景色
        .setLoadTextColor('blue') // 上拉文本的字体颜色
        .setLoadTextSize(25) // 上拉文本的字体大小
        .setLoadTextPullUp1('请继续上拉...') // 上拉1阶段文本
        .setLoadTextPullUp2('释放即可刷新') // 上拉2阶段文本
        .setLoadTextLoading('加载中...') // 上拉加载更多中时的文本

      expect(refreshConfigurator.getHasRefresh()).assertTrue();
      expect(refreshConfigurator.getHasLoadMore()).assertTrue();
      expect(refreshConfigurator.getMaxTranslate()).assertEqual(150);
      expect(refreshConfigurator.getSensitivity()).assertEqual(1);
      expect(refreshConfigurator.getListIsPlacement()).assertEqual(false);
      expect(refreshConfigurator.getAnimDuration()).assertEqual(300);
      expect(refreshConfigurator.getRefreshHeight()).assertEqual(80);
      expect(refreshConfigurator.getRefreshColor()).assertEqual('#ff0000');
      expect(refreshConfigurator.getRefreshBackgroundColor()).assertEqual('#ffbbfaf5');
      expect(refreshConfigurator.getRefreshTextColor()).assertEqual('red');
      expect(refreshConfigurator.getRefreshTextSize()).assertEqual(25);
      expect(refreshConfigurator.getRefreshAnimDuration()).assertEqual(1000);
      expect(refreshConfigurator.getLoadImgHeight()).assertEqual(50);
      expect(refreshConfigurator.getLoadBackgroundColor()).assertEqual('#ffbbfaf5');
      expect(refreshConfigurator.getLoadTextColor()).assertEqual('blue');
      expect(refreshConfigurator.getLoadTextSize()).assertEqual(25);
      expect(refreshConfigurator.getLoadTextPullUp1()).assertEqual('请继续上拉...');
      expect(refreshConfigurator.getLoadTextPullUp2()).assertEqual('释放即可刷新');
      expect(refreshConfigurator.getLoadTextLoading()).assertEqual('加载中...');
    })

    // 目前PullToRefresh等自定义组件内容XTS测试用例无法进行测试

  })
}